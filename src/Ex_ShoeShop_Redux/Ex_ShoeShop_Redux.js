import React, { Component } from "react";
import DetailShoe from "./DetailShoe";
import Cart from "./Cart";
import ListShoe from "./ListShoe";

export default class Ex_ShoeShop_Redux extends Component {
  render() {
    return (
      <div className="container py-5">
        <Cart />
        <ListShoe />
        <DetailShoe />
      </div>
    );
  }
}
