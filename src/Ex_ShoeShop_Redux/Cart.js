import React, { Component } from "react";
import { connect } from "react-redux";

class Cart extends Component {
  renderTbody = () => {
    return this.props.gioHang?.map((item) => {
      return (
        <tr>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>
            <img style={{ width: "50px" }} src={item.image} alt="" />
          </td>
          <td>{item.price * item.number}$</td>
          <td>
            <button
              onClick={this.props.handleTangGiamSL(item.id, -1)}
              className="btn btn-danger"
            >
              -
            </button>
            <span className="mx-2">{item.number}</span>
            <button
              onClick={() => {
                this.props.handleTangGiamSL(item.id, +1);
              }}
              className="btn btn-success"
            >
              +
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <table className="table">
        <thead>
          <tr>
            <td>Id</td>
            <td>Name</td>
            <td>Img</td>
            <td>Price</td>
            <td>Quantity(số lượng)</td>
          </tr>
        </thead>
        <tbody>{this.renderTbody()}</tbody>
      </table>
    );
  }
}
let mapStateToProps = (state) => {
  return {
    gioHang: state.shoeReducer.cart,
  };
};
let mapDispatchToProps = (dispatch) => {
  return {
    handleTangGiamSL: (id, soLuong) => {
      let action = {
        type: "TANG_GIAM_SO_LUONG",
        payload: {
          idShoe: id,
          soLuong: soLuong,
        },
      };
      dispatch(action);
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Cart);
