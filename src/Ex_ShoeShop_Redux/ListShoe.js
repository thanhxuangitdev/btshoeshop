import React, { Component } from "react";
import ItemShoe from "./ItemShoe";
import { connect } from "react-redux";

class ListShoe extends Component {
  renderListShoe = () => {
    return this.props.shoeArr.map((item, index) => {
      return <ItemShoe data={item} key={index} />;
    });
  };
  render() {
    return <div className="row">{this.renderListShoe()}</div>;
  }
}
let mapStateToProps = (state) => {
  return {
    ListShoe: state.shoeReducer.shoeArr,
  };
};

export default connect(mapStateToProps)(ListShoe);
